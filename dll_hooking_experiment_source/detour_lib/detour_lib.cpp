// detour_lib.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "detour_lib.h"
#include <iostream>
#include <tchar.h>
#include "detour_enums.h"
#include <Winternl.h>

//This is the handle for the IPC
HANDLE handle;


//This logs an API call via ipc
void spy_log(API_ID id);

//This creates the inter-process communication channel
bool spy_create_ipc();

//Hooked API functions
//MessageBoxW hook
int WINAPI HookedMessageBoxW(HWND hWnd, LPCWSTR lpText, LPCWSTR lpCaption, UINT uType,
	WORD wLanguageId, DWORD dwMilliseconds);


/*
// NtReadFile Hook
NTSTATUS NTAPI HookedNtReadFile(
	_In_     HANDLE           FileHandle,
	_In_opt_ HANDLE           Event,
	_In_opt_ PIO_APC_ROUTINE  ApcRoutine,
	_In_opt_ PVOID            ApcContext,
	_Out_    PIO_STATUS_BLOCK IoStatusBlock,
	_Out_    PVOID            Buffer,
	_In_     ULONG            Length,
	_In_opt_ PLARGE_INTEGER   ByteOffset,
	_In_opt_ PULONG           Key
);

//RegOpenKeyExA Hook
LSTATUS APIENTRY HookedRegOpenKeyExA(
	_In_ HKEY hKey,
	_In_opt_ LPCSTR lpSubKey,
	_In_opt_ DWORD ulOptions,
	_In_ REGSAM samDesired,
	_Out_ PHKEY phkResult
);
//RegQueryValueExA Hook
LSTATUS APIENTRY HookedRegQueryValueExA(
	_In_ HKEY hKey,
	_In_opt_ LPCSTR lpValueName,
	_Reserved_ LPDWORD lpReserved,
	_Out_opt_ LPDWORD lpType,
	_Out_writes_bytes_to_opt_(*lpcbData, *lpcbData) __out_data_source(REGISTRY) LPBYTE lpData,
	_When_(lpData == NULL, _Out_opt_) _When_(lpData != NULL, _Inout_opt_) LPDWORD lpcbData
);

//NtFreeVirtualMemory
NTSTATUS NTAPI HookedNtFreeVirtualMemory(
	HANDLE  ProcessHandle,
	PVOID   *BaseAddress,
	PSIZE_T RegionSize,
	ULONG   FreeType
);

//RegSetValueExA
LSTATUS APIENTRY HookedRegSetValueExA(
	_In_ HKEY hKey,
	_In_opt_ LPCSTR lpValueName,
	_Reserved_ DWORD Reserved,
	_In_ DWORD dwType,
	_In_reads_bytes_opt_(cbData) CONST BYTE* lpData,
	_In_ DWORD cbData
);

//CreateProcessInternal - TODO: very hard to find documentation, best guess:
DWORD WINAPI HookedCreateProcessInternalW(
	__in         DWORD unknown1,
	__in_opt     LPCTSTR lpApplicationName,
	__inout_opt  LPTSTR lpCommandLine,
	__in_opt     LPSECURITY_ATTRIBUTES lpProcessAttributes,
	__in_opt     LPSECURITY_ATTRIBUTES lpThreadAttributes,
	__in         BOOL bInheritHandles,
	__in         DWORD dwCreationFlags,
	__in_opt     LPVOID lpEnvironment,
	__in_opt     LPCTSTR lpCurrentDirectory,
	__in         LPSTARTUPINFO lpStartupInfo,
	__out        LPPROCESS_INFORMATION lpProcessInformation,
	__in         DWORD unknown2
);

*/

//Hook engine functions
BOOL(__cdecl *HookFunction)(ULONG_PTR OriginalFunction, ULONG_PTR NewFunction);
VOID(__cdecl *UnhookFunction)(ULONG_PTR Function);
ULONG_PTR(__cdecl *GetOriginalFunction)(ULONG_PTR Hook);



/***********************************************************/
void spy_log(API_ID id)
{
	char wb = (char)id;
	DWORD bytes_written;
	BOOL res = WriteFile(handle,
		&wb,
		1,
		&bytes_written,
		NULL);
	if (!res || bytes_written != 1)
	{
		std::cout << "Spy log write failed.\n";
	}
}
/***********************************************************/
bool spy_create_ipc()
{
	handle = CreateFile(L"\\\\.\\mailslot\\spy_log",
		GENERIC_WRITE,
		FILE_SHARE_READ,
		0,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		0);

	if (INVALID_HANDLE_VALUE == handle)
	{
		std::cout << "Spy failed to open IPC.\n";
		return false;
	}

	return true;
}

/***********************************************************/
void spy_init()
{
	std::cout << "Spy injection starting.." << std::endl;

	//Establish the loggin pipe
	if (!spy_create_ipc())
	{
		std::cout << "Spy interprocess comm creation failed.\n";
		return;
	}

	//Load the hooking engine
	HMODULE hHookEngineDll = LoadLibrary(_T("NtHookEngine.dll"));

	HookFunction = (BOOL(__cdecl *)(ULONG_PTR, ULONG_PTR))
		GetProcAddress(hHookEngineDll, "HookFunction");

	UnhookFunction = (VOID(__cdecl *)(ULONG_PTR))
		GetProcAddress(hHookEngineDll, "UnhookFunction");

	GetOriginalFunction = (ULONG_PTR(__cdecl *)(ULONG_PTR))
		GetProcAddress(hHookEngineDll, "GetOriginalFunction");

	//If we couldn't retrieve the hook functions, bail
	if (HookFunction == NULL || UnhookFunction == NULL ||
		GetOriginalFunction == NULL)
	{
		std::cout << "Spy failed to load hooking functions.\n";
		return;
	}
	std::cout << "Spy loaded hook engine, hooking.." << std::endl;


	//Hook the message box function
	HookFunction((ULONG_PTR)GetProcAddress(LoadLibrary(_T("User32.dll")), "MessageBoxTimeoutW"), (ULONG_PTR)&HookedMessageBoxW);

	/*
	//Hook NtReadFile function
	HookFunction((ULONG_PTR)GetProcAddress(LoadLibrary(_T("Ntdll.dll")), "NtReadFile"), (ULONG_PTR)&HookedNtReadFile);

	//Hook RegOpenKeyExA
	HookFunction((ULONG_PTR)GetProcAddress(LoadLibrary(_T("Advapi32.dll")), "RegOpenKeyExA"), (ULONG_PTR)&HookedRegOpenKeyExA);

	//Hook RegQueryValueExA
	HookFunction((ULONG_PTR)GetProcAddress(LoadLibrary(_T("Advapi32.dll")), "RegQueryValueExA"), (ULONG_PTR)&HookedRegQueryValueExA);

	//Hook NtFreeVirtualMemory
	//TODO: questionable if this techinique works on kernel-level functions? no userspace equivalent?
	HookFunction((ULONG_PTR)GetProcAddress(LoadLibrary(_T("NtosKrnl.exe")), "NtFreeVirtualMemory"), (ULONG_PTR)&HookedNtFreeVirtualMemory);

	//Hook RegSetValueExA
	HookFunction((ULONG_PTR)GetProcAddress(LoadLibrary(_T("Advapi32.dll")), "RegSetValueExA"), (ULONG_PTR)&HookedRegSetValueExA);

	//CreateProcessInternalW
	HookFunction((ULONG_PTR)GetProcAddress(LoadLibrary(_T("Kernel32.dll")), "CreateProcesssInternalW"), (ULONG_PTR)&HookedCreateProcessInternalW);

	std::cout << "Spy hooking completed.\n";
	*/
}


/******************** Hooked Functions ******************************/
int WINAPI HookedMessageBoxW(HWND hWnd, LPCWSTR lpText, LPCWSTR lpCaption, UINT uType,
	WORD wLanguageId, DWORD dwMilliseconds)
{
	//Pointer to the original function
	int (WINAPI *p_msg_func_ptr)(HWND hWnd, LPCWSTR lpText,
		LPCWSTR lpCaption, UINT uType, WORD wLanguageId,
		DWORD dwMilliseconds);

	//Populate with the original function
	p_msg_func_ptr = (int (WINAPI *)(HWND, LPCWSTR, LPCWSTR, UINT, WORD, DWORD))
		GetOriginalFunction((ULONG_PTR)HookedMessageBoxW);

	//Log this call
	spy_log(MessageBoxW_ID);

	//Call the original function
	return p_msg_func_ptr(hWnd, L"Hijacked!!!", L"Hooked this message.\n",
		uType, wLanguageId, dwMilliseconds);
}

/*
NTSTATUS WINAPI HookedNtReadFile(
	_In_     HANDLE           FileHandle,
	_In_opt_ HANDLE           Event,
	_In_opt_ PIO_APC_ROUTINE  ApcRoutine,
	_In_opt_ PVOID            ApcContext,
	_Out_    PIO_STATUS_BLOCK IoStatusBlock,
	_Out_    PVOID            Buffer,
	_In_     ULONG            Length,
	_In_opt_ PLARGE_INTEGER   ByteOffset,
	_In_opt_ PULONG           Key
)
{
	//Pointer to the original function
	NTSTATUS(WINAPI *p_msg_func_ptr)(
		HANDLE  FileHandle,
		HANDLE           Event,
		PIO_APC_ROUTINE  ApcRoutine,
		PVOID            ApcContext,
		PIO_STATUS_BLOCK IoStatusBlock,
		PVOID            Buffer,
		ULONG            Length,
		PLARGE_INTEGER   ByteOffset,
		PULONG           Key);

	//Populate with the original function
	p_msg_func_ptr = (NTSTATUS (WINAPI *) (HANDLE, HANDLE, PIO_APC_ROUTINE, PVOID, PIO_STATUS_BLOCK, PVOID, ULONG, PLARGE_INTEGER, PULONG))
		GetOriginalFunction((ULONG_PTR)HookedNtReadFile);

	//Log this call
	spy_log(NtReadFile_ID);

	//Call the original function
	return p_msg_func_ptr(NULL, Event, ApcRoutine, ApcContext, IoStatusBlock, Buffer, Length, ByteOffset, Key);
}


LSTATUS APIENTRY HookedRegOpenKeyExA(
	_In_ HKEY hKey,
	_In_opt_ LPCSTR lpSubKey,
	_In_opt_ DWORD ulOptions,
	_In_ REGSAM samDesired,
	_Out_ PHKEY phkResult
)
{
	//Pointer for function
	LSTATUS(APIENTRY *p_msg_func_ptr)(
		HKEY hKey,
		LPCSTR lpSubKey,
		DWORD ulOptions,
		REGSAM samDesired,
		PHKEY phkResult
		);

	//Populate with the original function
	p_msg_func_ptr = (LSTATUS(APIENTRY *) (HKEY, LPCSTR, DWORD, REGSAM, PHKEY))
		GetOriginalFunction((ULONG_PTR)HookedRegOpenKeyExA);

	//Log this call
	spy_log(RegOpenKeyExA_ID);

	//Call the original function
	return p_msg_func_ptr(hKey, lpSubKey, ulOptions, samDesired, phkResult);
}

LSTATUS APIENTRY HookedRegQueryValueExA(
	_In_ HKEY hKey,
	_In_opt_ LPCSTR lpValueName,
	_Reserved_ LPDWORD lpReserved,
	_Out_opt_ LPDWORD lpType,
	_Out_writes_bytes_to_opt_(*lpcbData, *lpcbData) __out_data_source(REGISTRY) LPBYTE lpData,
	_When_(lpData == NULL, _Out_opt_) _When_(lpData != NULL, _Inout_opt_) LPDWORD lpcbData
)
{
	//Pointer for function
	LSTATUS(APIENTRY * p_msg_func_ptr)(
		HKEY hKey,
		LPCSTR lpValueName,
		LPDWORD lpReserved,
		LPDWORD lpType,
		LPBYTE lpData,
		LPDWORD lpcbData
		);

	//Populate the pointer with the original function
	p_msg_func_ptr = (LSTATUS(APIENTRY *)(HKEY, LPCSTR, LPDWORD, LPDWORD, LPBYTE, LPDWORD))
		GetOriginalFunction((ULONG_PTR)HookedRegQueryValueExA);

	//Log this call
	spy_log(RegQueryValueExA_ID);

	//Call the original function
	return p_msg_func_ptr(hKey, lpValueName, lpReserved, lpType, lpData, lpcbData);
}

NTSTATUS NTAPI HookedNtFreeVirtualMemory(
	HANDLE  ProcessHandle,
	PVOID   *BaseAddress,
	PSIZE_T RegionSize,
	ULONG   FreeType
)
{
	//Pointer for the function
	NTSTATUS(NTAPI *p_msg_func_ptr)(
		HANDLE  ProcessHandle,
		PVOID   *BaseAddress,
		PSIZE_T RegionSize,
		ULONG   FreeType);
	//Populate the pointer with the original function
	p_msg_func_ptr = (NTSTATUS(NTAPI *)(HANDLE, PVOID*, PSIZE_T, ULONG))
		GetOriginalFunction((ULONG_PTR)HookedNtFreeVirtualMemory);

	//Log this call
	spy_log(NtFreeVirtualMemory_ID);

	//Call the original function
	return p_msg_func_ptr(ProcessHandle, BaseAddress, RegionSize, FreeType);
}


LSTATUS APIENTRY HookedRegSetValueExA(
	_In_ HKEY hKey,
	_In_opt_ LPCSTR lpValueName,
	_Reserved_ DWORD Reserved,
	_In_ DWORD dwType,
	_In_reads_bytes_opt_(cbData) CONST BYTE* lpData,
	_In_ DWORD cbData
)
{
	//Pointer for the function
	NTSTATUS(NTAPI  *p_msg_func_ptr)(
		_In_ HKEY hKey,
		_In_opt_ LPCSTR lpValueName,
		_Reserved_ DWORD Reserved,
		_In_ DWORD dwType,
		_In_reads_bytes_opt_(cbData) CONST BYTE* lpData,
		_In_ DWORD cbData
		);

	//Populatethe pointer with the original function
	p_msg_func_ptr = (NTSTATUS(NTAPI*)(HKEY, LPCSTR, DWORD, DWORD, CONST BYTE*, DWORD))
		GetOriginalFunction((ULONG_PTR)HookedRegSetValueExA);

	//Log this call
	spy_log(NtFreeVirtualMemory_ID);

	//Call the original function
	return p_msg_func_ptr(hKey, lpValueName, Reserved, dwType, lpData, cbData);
}

DWORD WINAPI HookedCreateProcessInternalW(
	__in         DWORD unknown1,
	__in_opt     LPCTSTR lpApplicationName,
	__inout_opt  LPTSTR lpCommandLine,
	__in_opt     LPSECURITY_ATTRIBUTES lpProcessAttributes,
	__in_opt     LPSECURITY_ATTRIBUTES lpThreadAttributes,
	__in         BOOL bInheritHandles,
	__in         DWORD dwCreationFlags,
	__in_opt     LPVOID lpEnvironment,
	__in_opt     LPCTSTR lpCurrentDirectory,
	__in         LPSTARTUPINFO lpStartupInfo,
	__out        LPPROCESS_INFORMATION lpProcessInformation,
	__in         DWORD unknown2
)
{
	//Pointer for the function
	DWORD(WINAPI * p_msg_func_ptr)(
		__in         DWORD unknown1,
		__in_opt     LPCTSTR lpApplicationName,
		__inout_opt  LPTSTR lpCommandLine,
		__in_opt     LPSECURITY_ATTRIBUTES lpProcessAttributes,
		__in_opt     LPSECURITY_ATTRIBUTES lpThreadAttributes,
		__in         BOOL bInheritHandles,
		__in         DWORD dwCreationFlags,
		__in_opt     LPVOID lpEnvironment,
		__in_opt     LPCTSTR lpCurrentDirectory,
		__in         LPSTARTUPINFO lpStartupInfo,
		__out        LPPROCESS_INFORMATION lpProcessInformation,
		__in         DWORD unknown2
	);

	//Populatethe pointer with the original function
	p_msg_func_ptr = (DWORD(WINAPI*)(DWORD,
						LPCTSTR,
						LPTSTR,
						LPSECURITY_ATTRIBUTES,
						LPSECURITY_ATTRIBUTES,
						BOOL,
						DWORD,
						LPVOID,
						LPCTSTR,
						LPSTARTUPINFO,
						LPPROCESS_INFORMATION,
						DWORD))
		GetOriginalFunction((ULONG_PTR)HookedCreateProcessInternalW);

	//Log this call
	spy_log(CreateProcessInternalW_ID);

	//Call the original function
	return p_msg_func_ptr(unknown1, lpApplicationName, lpCommandLine,
		lpProcessAttributes, lpThreadAttributes, bInheritHandles, dwCreationFlags,
		lpEnvironment, lpCurrentDirectory, lpStartupInfo, lpProcessInformation, unknown2);
}
*/