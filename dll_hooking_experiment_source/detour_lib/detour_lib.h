#pragma once

#ifdef DETOURLIB_EXPORTS
#define DETOURLIB_API __declspec(dllexport)
#else
#define DETOURLIB_API __declspec(dllimport)
#endif

extern "C" DETOURLIB_API void spy_init();
extern "C" DETOURLIB_API void spy_cleanup();