// test_app.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "stdafx.h"

int main()
{
	Sleep(10);
    std::cout << "Test app alive\n";
	std::cout << "Test app creating popup\n";

	while (true)
	{
		MessageBox(0, L"Test app message box.\n", L"Test App Msg", MB_ICONINFORMATION);
		Sleep(5000);
	}
}
