#include  "pch.h"
#include <iostream>
#include <Windows.h>
#include "detour_enums.h"
#include "MalwareClassifier.h"

//Target path
wchar_t target_proc_path[] = L"..\\Debug\\test_app.exe";

//DLL path
wchar_t dll_path[] = L"..\\Debug\\detour_lib.dll";

//Target startup process info
STARTUPINFO             startup_info;

//Target process info
PROCESS_INFORMATION     process_info;

//IPC handle
HANDLE handle;

//This is the array of current detections
double detections[API_ID_LAST];

//This is the malware classifier
MalwareClassifier classifier;

//This function injects a DLL
BOOL WINAPI inject(__in LPCWSTR dll_path, __in LPCWSTR target_proc_path);


//This function processes a log entry sent via IPC
void process_entry(API_ID id);

//This function listens to the logging IPC for data
bool log_data();

//This functions configures the IPC mechanism
bool setup_ipc();

/***********************************************************/
BOOL WINAPI inject(__in LPCWSTR dll_path, __in LPCWSTR target_proc_path)
{
	std::cout << "Injection started..";
	SIZE_T dll_name_len;
	LPVOID load_library_w_addr = NULL;
	LPVOID remote_dll_str;

	memset(&startup_info, 0, sizeof(startup_info));
	startup_info.cb = sizeof(STARTUPINFO);


	// Create the remote process
	if (!CreateProcess(target_proc_path, NULL, NULL, NULL, FALSE,
		 CREATE_SUSPENDED, NULL, NULL, &startup_info, &process_info))

	{
		std::cout << "Failed to start target process.\n";
		return FALSE;
	}

	// Get the address of LoadLibrary
	load_library_w_addr = GetProcAddress(GetModuleHandle(L"KERNEL32.DLL"), "LoadLibraryW");

	if (!load_library_w_addr)

	{
		std::cout << "Failed to get process address.\n";
		CloseHandle(process_info.hProcess);
		return FALSE;
	}

	// Allocate memory in the remote process for the injected DLL name
	dll_name_len = wcslen(dll_path) * sizeof(WCHAR);
	remote_dll_str = VirtualAllocEx(process_info.hProcess, NULL, dll_name_len + 1, MEM_COMMIT, PAGE_READWRITE);

	if (!remote_dll_str)
	{
		std::cout << "Failed to allocate process memory for DLL name.\n";
		CloseHandle(process_info.hProcess);
		return FALSE;
	}

	// Write the DLL name into the remote process memory
	if (!WriteProcessMemory(process_info.hProcess, remote_dll_str, dll_path, dll_name_len, NULL)) {

		std::cout << "Failed to get write process memory\n";
		VirtualFreeEx(process_info.hProcess, remote_dll_str, 0, MEM_RELEASE);
		CloseHandle(process_info.hProcess);
		return FALSE;
	}

	// Create a remote thread in the target process to load the DLL into the target process memory space
	HANDLE hThread = CreateRemoteThread(process_info.hProcess, NULL, NULL, (LPTHREAD_START_ROUTINE)load_library_w_addr, remote_dll_str, NULL, NULL);
	if (!hThread) {

		std::cout << "Failed to create remote thread\n";

	}
	else 
	{
		// Wait for the remote thread to finish injecting the DLL
		WaitForSingleObject(hThread, INFINITE);

		std::cout << "Injected thread finished, resuming target process..\n";

		//resume suspended process
		ResumeThread(process_info.hThread);
	}
	
	//Free the allocated memory and clean up
	VirtualFreeEx(process_info.hProcess, remote_dll_str, 0, MEM_RELEASE);
	CloseHandle(process_info.hProcess);
	std::cout << "Injection done.\n";
	return TRUE;
}

/***********************************************************/
bool setup_ipc()
{
	handle = CreateMailslot(L"\\\\.\\mailslot\\spy_log", 1, MAILSLOT_WAIT_FOREVER, NULL);
	
	if (INVALID_HANDLE_VALUE == handle)
	{
		std::cout << "Injection setup IPC failed.\n";
		return false;
	}
	return true;
}

/***********************************************************/
void process_entry(API_ID id)
{
	switch (id)
	{
		case MessageBoxW_ID:
		{
			std::cout << "Target process used MessageBoxW API\n";
			break;
		}
	}
}

/***********************************************************/
bool log_data()
{
	std::cout << "Injection logging started.\n";
	BOOL result;
	char buff;
	DWORD rd_bytes;
	bool do_break = false;
	while (!do_break) 
	{
		//Read client message
		result = ReadFile(handle, &buff, 1, &rd_bytes, NULL);

		if ((!result) || (0 == rd_bytes))
		{
			std::cout << "Injection read file failed.\n";
			return false;
		}

		API_ID id = (API_ID) (buff & 0xFF);
		process_entry(id);
	}

	return true;
}
/***********************************************************/
int main()
{
	std::cout << "Injection app starting..\n";
	
	//Set up the IPC
	setup_ipc();

	//Inject the spy DLL
	bool result = inject(dll_path, target_proc_path);
	if (!result)
	{
		std::cout << "Injection failed.\n";
	}

	//Start logging and parsing the data
	if (!log_data())
	{
		std::cout << "Injection logging failed.\n";
	}
}