/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * CompactClassificationTree.h
 *
 * Code generation for function 'CompactClassificationTree'
 *
 */

#ifndef COMPACTCLASSIFICATIONTREE_H
#define COMPACTCLASSIFICATIONTREE_H

/* Include files */
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
double c_CompactClassificationTree_pre(
    const double obj_CutPredictorIndex[55], const double obj_Children[110],
    const double obj_CutPoint[55], const double obj_PruneList_data[],
    const boolean_T obj_NanCutPoints[55], const double obj_Cost[4],
    const double obj_ClassProbability[110], const double Xin[20]);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (CompactClassificationTree.h) */
