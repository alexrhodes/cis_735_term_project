/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * classify_app_data.h
 *
 * Code generation for function 'classify_app_data'
 *
 */

#ifndef CLASSIFY_APP_DATA_H
#define CLASSIFY_APP_DATA_H

/* Include files */
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#endif
/* End of code generation (classify_app_data.h) */
