/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * CompactClassificationTree.c
 *
 * Code generation for function 'CompactClassificationTree'
 *
 */

/* Include files */
#include "CompactClassificationTree.h"
#include "rt_nonfinite.h"
#include "rt_nonfinite.h"

/* Function Definitions */
double c_CompactClassificationTree_pre(
    const double obj_CutPredictorIndex[55], const double obj_Children[110],
    const double obj_CutPoint[55], const double obj_PruneList_data[],
    const boolean_T obj_NanCutPoints[55], const double obj_Cost[4],
    const double obj_ClassProbability[110], const double Xin[20])
{
  double a__6_idx_0;
  double d;
  int m;
  boolean_T exitg1;
  m = 0;
  exitg1 = false;
  while (!(exitg1 || (obj_PruneList_data[m] <= 0.0))) {
    d = Xin[(int)obj_CutPredictorIndex[m] - 1];
    if (rtIsNaN(d) || obj_NanCutPoints[m]) {
      exitg1 = true;
    } else if (d < obj_CutPoint[m]) {
      m = (int)obj_Children[m << 1] - 1;
    } else {
      m = (int)obj_Children[(m << 1) + 1] - 1;
    }
  }
  d = obj_ClassProbability[m + 55];
  a__6_idx_0 = obj_ClassProbability[m] * obj_Cost[0] + d * obj_Cost[1];
  d = obj_ClassProbability[m] * obj_Cost[2] + d * obj_Cost[3];
  if ((a__6_idx_0 > d) || (rtIsNaN(a__6_idx_0) && (!rtIsNaN(d)))) {
    m = 1;
  } else {
    m = 0;
  }
  return m;
}

/* End of code generation (CompactClassificationTree.c) */
