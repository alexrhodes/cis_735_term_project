/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * classify_app_initialize.c
 *
 * Code generation for function 'classify_app_initialize'
 *
 */

/* Include files */
#include "classify_app_initialize.h"
#include "rt_nonfinite.h"

/* Function Definitions */
void classify_app_initialize(void)
{
}

/* End of code generation (classify_app_initialize.c) */
