#include "classify_counter.h"
#include "classify_app.h"
#include "pipe.h"

//This array holds detected APIs
double detections[API_ID_LAST] = 
{
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
};

static int reported = 0;

/*******************************************/
void classify_notify(API_ID id)
{
    if(id < 0 || id >= API_ID_LAST)
    {
        pipe("WARN:Invalid API ID passed to classifier");
        return;
    }
    //Store the detection
    detections[id] = 1.0;

    //Re-assess the sample
    double label = classify_app(detections);
    if(label == 1.0 && !reported)
    {
        pipe("INFO:ML labeled MALICIOUS!");
        reported = 1;
    }
    else if(label == 0.0 && !reported)
    {
        pipe("INFO:ML given %d, not malicous.\n", (int)id);
    }
}