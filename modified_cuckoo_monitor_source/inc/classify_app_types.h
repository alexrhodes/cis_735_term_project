/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * classify_app_types.h
 *
 * Code generation for function 'classify_app'
 *
 */

#ifndef CLASSIFY_APP_TYPES_H
#define CLASSIFY_APP_TYPES_H

/* Include files */
#include "rtwtypes.h"

#endif
/* End of code generation (classify_app_types.h) */
