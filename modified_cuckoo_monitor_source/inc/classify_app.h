/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * classify_app.h
 *
 * Code generation for function 'classify_app'
 *
 */

#ifndef CLASSIFY_APP_H
#define CLASSIFY_APP_H

/* Include files */
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
extern double classify_app(const double input_arr[20]);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (classify_app.h) */
