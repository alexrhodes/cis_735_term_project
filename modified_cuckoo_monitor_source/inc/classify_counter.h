#ifndef CLASSIFY_COUNTER
#define CLASSIFY_COUNTER

typedef enum
{
    NT_READFILE_ID,
    RegOpenKeyExA_ID,
    RegQueryValueExA_ID,
    NtFreeVirtualMemory_ID,
    RegSetValueExA_ID,
    CreateProcessInternalW_ID,
    LdrLoadDll_ID,
    SetFilePointer_ID,
    DelteFileW_ID,
    RegCreateKeyExA_ID,
    NtDelayExecution_ID,
    NtResumeThread_ID,
    NtCreateFile_ID,
    LdrGetProcedureAddress_ID,
    NtOpenThread_ID,
    NtDuplicateObject_ID,
    RegCloseKey_ID,
    CryptAcquireContextW_ID,
    OpenSCManagerA_ID,
    NtCreateThreadEx_ID,
    API_ID_LAST
}API_ID;

extern void classify_notify(API_ID id);


#endif