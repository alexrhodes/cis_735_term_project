/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * _coder_classify_app_info.h
 *
 * Code generation for function 'classify_app'
 *
 */

#ifndef _CODER_CLASSIFY_APP_INFO_H
#define _CODER_CLASSIFY_APP_INFO_H

/* Include files */
#include "mex.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
MEXFUNCTION_LINKAGE mxArray *emlrtMexFcnProperties(void);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (_coder_classify_app_info.h) */
