/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * _coder_classify_app_api.h
 *
 * Code generation for function 'classify_app'
 *
 */

#ifndef _CODER_CLASSIFY_APP_API_H
#define _CODER_CLASSIFY_APP_API_H

/* Include files */
#include "emlrt.h"
#include "tmwtypes.h"
#include <string.h>

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
real_T classify_app(real_T input_arr[20]);

void classify_app_api(const mxArray *prhs, const mxArray **plhs);

void classify_app_atexit(void);

void classify_app_initialize(void);

void classify_app_terminate(void);

void classify_app_xil_shutdown(void);

void classify_app_xil_terminate(void);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (_coder_classify_app_api.h) */
